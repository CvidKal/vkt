//
//  FastWrapper.hpp
//  markerARLib
//
//  Created by fg on 16/5/4.
//  Copyright © 2016年 CvidKal. All rights reserved.
//

#ifndef FastWrapper_hpp
#define FastWrapper_hpp

#include <stdio.h>
#include "fast.hpp"
#include <opencv2/opencv.hpp>

    namespace  vkt {

        class FastWrapper {


            struct GridOccupancy{
                GridOccupancy(const int _rows,const int _cols,int N):rows(_rows),cols(_cols){
                    grid = new int[rows*cols];
                    std::fill(grid,grid+rows*cols,N);
//                    memset(grid,0, sizeof(int)*rows*cols);
                }
                inline int& operator()(const int row,const int col){
                    return grid[col+ row*cols];
                }
                int rows,cols;
                int* grid;
                ~GridOccupancy(){
                    delete[] grid;
                }

                void printGrid(){
                    for (int i = 0; i < rows; ++i) {
                        for (int j = 0; j < cols; ++j) {
                            std::cout<<(*this)(i,j)<<" ";
                        }
                        std::cout<<std::endl;
                    }

                }
            };

    public:
        enum ScoreType{
            FASTScore = 1,
            HarrisScore = 0
        };

        FastWrapper(int threshold = 10,int scoreType = FASTScore) : b(threshold),scoreType(scoreType) {};

        void
        detect(const cv::Mat &img, std::vector<cv::KeyPoint> &kpts, const cv::Mat &_mask,const int GridRows,
               const int GridCols,const int featuresPerGrid, const int borderSize);
        void
        detect(const cv::Mat &img, std::vector<cv::KeyPoint> &kpts, const cv::Mat &_mask, const int maxNum,
                            const int borderSize);

    private:
        int b;
        int scoreType;

    };
    //TODO: implement harris corner score
    float
    shiTomasiScore(const cv::Mat& img, int u, int v);
}

#endif /* FastWrapper_hpp */
