//
// Created by fg on 8/19/16.
//

#ifndef ODOMETRY_TUM_READER_H
#define ODOMETRY_TUM_READER_H

#include <stdio.h>

#include <vector>
#include <fstream>
#include <string>
#include <iostream>
#include <iomanip>
#include <memory>


#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <opencv2/opencv.hpp>


using namespace std;


class TUM_Reader {
public:
    enum DataType{
        RGB = 1<<0,
        DEPTH = 1<<1,
        GT = 1<<2
    };

    public:
    typedef std::pair<double, string> Node;
    typedef std::pair<Node, Node> rgbdPair;
    typedef std::pair<double,Eigen::Affine3d> TrjacNode;

        TUM_Reader(const std::string&datasetPath):datasetPath(datasetPath){};

        void addData(const std::string&filePath,const DataType type){
            std::ifstream fin;
            fin.open(filePath,ios::in);
            if(!fin.good()){
                std::cerr<<"Fail to open rgb.txt"<<endl;
                return;
            }
            type_ |= type;
            std::string temp;
            getline(fin,temp);
            getline(fin,temp);
            getline(fin,temp);

            Node n;
            std::vector<Node> &dataVector = type==RGB?rgb:depth;
            while(fin.good()){
                std::string relativePath;
                fin>>n.first>>relativePath;
                n.second = datasetPath + "/" + relativePath;
                dataVector.push_back(n);
            }
        }

        void addGroundTruth(const std::string&gt){

        }



        bool getNextData(Node& next,const DataType type)
        {
            if(!(type&type_)){
                std::cerr<<"This type of data is empty!"<<endl;
                return false;
            }

            if(type == RGB){
                static int rgbI =0;
                if(rgbI == rgb.size()) {
                    std::cerr<<"No more data!"<<endl;
                    return false;
                }
                next = rgb[rgbI];
                rgbI++;

            }
            else if(type == DEPTH){
                static int depthI = 0;
                if(depthI == depth.size()) {
                    std::cerr<<"No more data!"<<endl;
                    return false;
                }
                next = rgb[depthI];
                depthI++;
            }
            return true;
        }

     bool getPairById(int i,Node& next,const DataType type){
         if(!(type&type_)){
             std::cerr<<"This type of data is empty!"<<endl;
             return false;
         }
         if(type == RGB){
             if(i>=rgb.size()){
                 std::cerr<<"Index is out of range!"<<endl;
                 return false;
             }
             next = rgb[i];
         }
         else if(type  == DEPTH){
             if(i>=depth.size()){
                 std::cerr<<"Index is out of range!"<<endl;
                 return false;
             }
             next = depth[i];
         }
    }

        ~TUM_Reader(){};
    protected:
        std::vector<Node> rgb;
        std::vector<Node> depth;
        std::vector<Node> gt;
        int type_;
        std::string datasetPath;
};



#endif //ODOMETRY_TUM_READER_H
