//
// Created by fg on 2016/12/14.
//

#ifndef VKT_TICKTOCK_H
#define VKT_TICKTOCK_H

#include <unordered_map>
#include <opencv2/opencv.hpp>

extern std::unordered_map<std::string, cv::TickMeter> timers;
#ifdef TICKTOCK
#define TICK(name) timers[name].start()
#define TOCK(name) timers[name].stop()
#define RETICK(name) timers[name].reset();timers[name].start()
#define GETTIMESEC(name) timers[name].getTimeSec()
#define GETTIMEMILLI(name) timers[name].getTimeMilli()
#else
#define TICK(name)
#define TOCK(name)
#define RETICK(name)
#define GETTIMESEC(name)
#define GETTIMEMILLI(name)
#endif



#endif //VKT_TICKTOCK_H
