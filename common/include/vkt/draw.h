#pragma once

#ifndef __DRAW_H_
#define __DRAW_H_

#include <opencv2/opencv.hpp>
#include <Eigen/Dense>
#include <vector>
using namespace cv;
using namespace std;


/*
	This function is used for drawing feature tracking line between former image and current image.
*/
Mat drawTrackLine(const Mat &img, const vector<KeyPoint> &origin, const vector<KeyPoint> &now, const vector<DMatch> &dmatch, Scalar color = CV_RGB(255, 255, 0), int thickness = 1);
Mat drawTrackLine(const Mat&img, const vector<Point2f> &origin, const vector<Point2f> &now, Scalar color = CV_RGB(255, 255, 0), int thickness = 1);
/*
	This funtion is designed for displaying Debug information
*/
Mat drawInfo(const Mat &img, const string &str, const Point2i &pos, Scalar color = CV_RGB(0, 255, 255), int thickness = 1);
void drawPoints(const Mat&img, const vector<Point2f> &pts2d, Mat &out);
void drawMatches(const Mat&left,const Mat&right,const vector<Eigen::Vector2f>&leftPoints,const vector<Eigen::Vector2f>&rightPoints,Mat&output);
void drawPoints(const Mat&img,const vector<KeyPoint>&kpts,const std::vector<bool>&mask,Mat&out);
Mat drawString(const Mat&img,const string&str,const int&row);

//void drawBoxAR(const Mat&img,)


#endif