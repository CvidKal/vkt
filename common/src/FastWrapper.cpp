//
//  FastWrapper.cpp
//  markerARLib
//
//  Created by fg on 16/5/4.
//  Copyright © 2016年 CvidKal. All rights reserved.
//

#include <vkt/FastWrapper.hpp>
#include <vkt/fast.hpp>
#include <algorithm>
using namespace std;
using namespace cv;


namespace vkt {

    void
    FastWrapper::detect(const cv::Mat &img, std::vector<cv::KeyPoint> &kpts, const cv::Mat &_mask, const int maxNum,
                        const int borderSize) {
        Mat mask;
        if (_mask.empty()) {
            mask = Mat::ones(img.size(), CV_8U);
        } else
            mask = _mask;
        kpts.clear();
        kpts.reserve(maxNum);
        if (img.cols < 2 * borderSize || img.rows < 2 * borderSize) {
            return;
        }
        Mat gray;
        if (img.channels() != 1) {
            cvtColor(img, gray, CV_BGR2GRAY);
        } else {
            gray = img;
        }

        int GridRows = gray.rows / 30 + 1;
        int GridCols = GridRows * gray.cols / gray.rows;
        int rowStep = 30;
        int colStep = gray.cols / GridCols;
        int numPerGrid = maxNum / (GridCols * GridRows) + 1;
//    GridRows = 1;
//    GridCols = 1;
        xy *corners;
        int n_Corners;
        corners = fast10_detect_nonmax(gray.ptr < unsigned
        char > (borderSize) + borderSize, gray.cols - 2 * borderSize, gray.rows -
                                                                      2 * borderSize, gray.step1(), b, &n_Corners);
        std::vector<xy>::iterator nit;
        std::vector<int> grid(GridCols * GridRows, numPerGrid);

#define PARTIAL_SORT
#ifdef PARTIAL_SORT
        if (2 * maxNum < n_Corners)
            std::partial_sort(corners, corners + 2 * maxNum, corners + n_Corners, [](const xy &a, const xy &b) {
                return a.score > b.score;
            });
        int numRes = maxNum;
        for (int i = 0; i < n_Corners && numRes > 0; ++i) {
            auto &pt = corners[i];
            pt.x += borderSize;
            pt.y += borderSize;
            if (mask.at<uchar>(pt.y, pt.x)) {
                int x = pt.x / colStep;
                int y = pt.y / rowStep;
                if (grid[x + y * GridCols]) {
                    kpts.push_back(KeyPoint(pt.x, pt.y, 7.f, -1, pt.score));
                    numRes--;
                    --grid[x + y * GridCols];
                }
            }
        }
#else
        if(2*maxNum < n_Corners)
            std::nth_element(corners,corners+2*maxNum,corners+n_Corners,[](const xy&a,const xy&b){
            return a.score > b.score;});
        int numRes = maxNum;
        int refScore = corners[2*maxNum].score;
        for (int i = 0; i < n_Corners&&numRes>0; ++i) {
            auto &pt = corners[i];
            if(pt.score<refScore)
                continue;
            pt.x += borderSize;
            pt.y += borderSize;
            int x = pt.x/colStep;
            int y = pt.y/rowStep;
            if(grid[x+y*GridCols]--){
                kpts.push_back(KeyPoint(pt.x,pt.y,7.f,-1,pt.score));
                numRes --;
            }
        }
#endif
        free(corners);

//#define DEBUG_GRID
#ifdef DEBUG_GRID

        for(int i=0;i<GridRows;++i){
            for (int j = 0; j < GridCols; ++j) {
                cout<<grid[j+ i * GridCols]<<" ";

            cout<<endl;
        }
        }
#endif

    }

    void
    FastWrapper::detect(const cv::Mat &img, std::vector<cv::KeyPoint> &kpts, const cv::Mat &_mask,const int GridRows,
           const int GridCols,const int featuresPerGrid, const int borderSize){

        Mat mask;
        if (_mask.empty()) {
            mask = Mat::ones(img.size(), CV_8U);
        } else
            mask = _mask;
        kpts.clear();
        kpts.reserve(featuresPerGrid*GridCols*GridRows);
        if (img.cols < 2 * borderSize || img.rows < 2 * borderSize) {
            return;
        }
        Mat gray;
        if (img.channels() != 1) {
            cvtColor(img, gray, CV_BGR2GRAY);
        } else {
            gray = img;
        }

        int rowStep = gray.rows/GridRows;
        int colStep = gray.cols/GridCols;

        xy *corners;
        int n_Corners;
        corners = fast10_detect_nonmax(gray.ptr <unsigned char> (borderSize) + borderSize, gray.cols - 2 * borderSize, gray.rows -
                                                                                                                       2 * borderSize, gray.step1(), b, &n_Corners);
        std::vector<xy>::iterator nit;
        std::vector<int> grid(GridCols * GridRows, featuresPerGrid);


#define PARTIAL_SORT
        int maxNum = featuresPerGrid * GridCols * GridRows;
#ifdef PARTIAL_SORT
        if (2 * maxNum < n_Corners)
            std::partial_sort(corners, corners + 2 * maxNum, corners + n_Corners, [](const xy &a, const xy &b) {
                return a.score > b.score;
            });
        int numRes = maxNum;
        for (int i = 0; i < n_Corners && numRes > 0; ++i) {
            auto &pt = corners[i];
            pt.x += borderSize;
            pt.y += borderSize;
            if (mask.at<uchar>(pt.y, pt.x)) {
                int x = pt.x / colStep;
                int y = pt.y / rowStep;
                if (grid[x + y * GridCols]) {
                    kpts.push_back(KeyPoint(pt.x, pt.y, 7.f, -1, pt.score));
                    numRes--;
                    grid[x + y * GridCols]--;
                }
            }
        }
#else
        if(2*maxNum < n_Corners)
            std::nth_element(corners,corners+2*maxNum,corners+n_Corners,[](const xy&a,const xy&b){
            return a.score > b.score;});
        int numRes = maxNum;
        int refScore = corners[2*maxNum].score;
        for (int i = 0; i < n_Corners&&numRes>0; ++i) {
            auto &pt = corners[i];
            if(pt.score<refScore)
                continue;
            pt.x += borderSize;
            pt.y += borderSize;
            int x = pt.x/colStep;
            int y = pt.y/rowStep;
            if(grid[x+y*GridCols]--){
                kpts.push_back(KeyPoint(pt.x,pt.y,7.f,-1,pt.score));
                numRes --;
            }
        }
#endif
        free(corners);


    }



    }